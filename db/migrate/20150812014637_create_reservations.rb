class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :client_id
      t.integer :adults
      t.integer :kids
      t.integer :babies
      t.string :origin
      t.string :motive
      t.date :checkin
      t.date :checkout
      t.integer :payment
      t.text :observations

      t.timestamps null: false
    end
  end
end
