# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160122175243) do

  create_table "clients", force: :cascade do |t|
    t.string   "documentation", limit: 255
    t.string   "name",          limit: 255
    t.string   "company",       limit: 255
    t.string   "email",         limit: 255
    t.string   "telephone",     limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "events", force: :cascade do |t|
    t.integer  "client_id",           limit: 4
    t.string   "date_event",          limit: 255
    t.string   "event_type",          limit: 255
    t.string   "quantity",            limit: 255
    t.string   "begin_date",          limit: 255
    t.string   "end_date",            limit: 255
    t.string   "begin_time",          limit: 255
    t.string   "end_time",            limit: 255
    t.string   "assembly_type",       limit: 255
    t.text     "event_description",   limit: 65535
    t.boolean  "cold_snacks",         limit: 1
    t.boolean  "hot_snacks",          limit: 1
    t.boolean  "dish_menu",           limit: 1
    t.boolean  "buffet_type",         limit: 1
    t.boolean  "theme_stations",      limit: 1
    t.boolean  "morning_coffeebreak", limit: 1
    t.boolean  "evening_coffeebreak", limit: 1
    t.boolean  "cheese_table",        limit: 1
    t.boolean  "uncorking_coctels",   limit: 1
    t.boolean  "uncorking_whisky",    limit: 1
    t.boolean  "uncorking_wine",      limit: 1
    t.boolean  "uncorking_vodka",     limit: 1
    t.boolean  "coctel_bar",          limit: 1
    t.boolean  "sodas",               limit: 1
    t.string   "color_theme",         limit: 255
    t.boolean  "flower_arrengements", limit: 1
    t.boolean  "baloons",             limit: 1
    t.boolean  "toldo",               limit: 1
    t.boolean  "sound",               limit: 1
    t.boolean  "dj",                  limit: 1
    t.boolean  "musical_group",       limit: 1
    t.boolean  "podium",              limit: 1
    t.boolean  "videobeam",           limit: 1
    t.boolean  "lights",              limit: 1
    t.boolean  "inflatable",          limit: 1
    t.boolean  "magic_show",          limit: 1
    t.boolean  "dance_show",          limit: 1
    t.boolean  "crazy_hour",          limit: 1
    t.text     "other_services",      limit: 65535
    t.text     "observations",        limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "img",        limit: 255
    t.integer  "room_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "room_id",        limit: 4
    t.integer  "quantity",       limit: 4
    t.integer  "reservation_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.integer  "client_id",    limit: 4
    t.integer  "adults",       limit: 4
    t.integer  "kids",         limit: 4
    t.integer  "babies",       limit: 4
    t.string   "origin",       limit: 255
    t.string   "motive",       limit: 255
    t.date     "checkin"
    t.date     "checkout"
    t.integer  "payment",      limit: 4
    t.text     "observations", limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.float    "price",       limit: 24
    t.text     "description", limit: 65535
    t.string   "cover",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "permalink",   limit: 255
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "permalink",              limit: 255
    t.string   "username",               limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
