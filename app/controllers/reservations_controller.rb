#Generado con Keppler.
class ReservationsController < ApplicationController
  before_filter :authenticate_user!, except: [:new, :create]
  layout :resolve_layout
  load_and_authorize_resource except: [:new, :create]
  before_action :set_reservation, only: [:show, :edit, :update, :destroy]

  # GET /reservations
  def index
    reservations = Reservation.searching(@query).all
    @objects, @total = reservations.page(@current_page), reservations.size
    redirect_to reservations_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /reservations/1
  def show
  end

  # GET /reservations/new
  def new
    @reservation = Reservation.new(checkin: params[:checkin], checkout: params[:checkout])
    @reservation.client = Client.new
  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  def create
    @reservation = Reservation.new(reservation_params)
    respond_to do |format|
      if @reservation.save
        ReservationMailer.reservation_admin(@reservation).deliver_now
        ReservationMailer.reservation_user(@reservation).deliver_now
        format.html { redirect_to root_path, notice: @reservation.client.name+', La solicitud de reserva ya ha sido enviada.' }
        format.json { render action: 'show', status: :created, location: @reservation }
      else
        format.html { render action: 'new' }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  def update
    if @reservation.update(reservation_params)
      redirect_to @reservation, notice: 'Reservation was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /reservations/1
  def destroy
    @reservation.destroy
    redirect_to reservations_url, notice: 'Reservation was successfully destroyed.'
  end

  def destroy_multiple
    Reservation.destroy redefine_ids(params[:multiple_ids])
    redirect_to reservations_path(page: @current_page, search: @query), notice: "Reservaciones eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def reservation_params
      params.require(:reservation).permit(:adults, :kids, :babies, :origin, :motive, :checkin, :checkout, :payment, :observations, orders_attributes: [:id, :room_id, :quantity, :_destroy], client_attributes: [:name, :documentation, :telephone, :email])
    end

    def resolve_layout
      case action_name
      when "destroy", "update", "edit", "show", "index"
        'admin/application'
      else
        'frontend/application'
      end
    end

end
