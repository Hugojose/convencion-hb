class FrontendController < ApplicationController
	layout 'layouts/frontend/application'
	
  def index
  	@rooms = Room.all.shuffle[0...4]
  end

  def show_room
  	@room = Room.find_by permalink: params[:permalink]
  	@rooms = Room.all
  end

  def restaurants
  	
  end

end
