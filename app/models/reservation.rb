#Generado por keppler
require 'elasticsearch/model'
class Reservation < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_many :rooms, through: :orders
  has_many :orders
  belongs_to :client
  accepts_nested_attributes_for :orders, :reject_if => :all_blank, :allow_destroy => true
  validates_presence_of :adults, :checkin, :checkout
  validate :validates_rooms, :validate_fields, :valid_check_in, :valid_date_range_required, :orders_filter
  accepts_nested_attributes_for :client
  before_validation :verify_client

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query(query)).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:origin, :motive, :checkin, :checkout, :payment, :client_name, :client_documentation, :client_telephone, :client_email], operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      client_name: self.client.name,
      client_documentation: self.client.documentation,
      client_telephone: self.client.telephone,
      client_email: self.client.email,
      adults: self.adults.to_s,
      kids: self.kids.to_s,
      babies: self.babies.to_s,
      origin: self.origin,
      motive: self.motive,
      checkin:  self.checkin.strftime("%d-%m-%Y"),
      checkout:  self.checkout.strftime("%d-%m-%Y"),
      payment:  self.payment.to_s,
      observations:  self.observations
    }.as_json
  end

  def reservation_days
    (self.checkout.to_date - self.checkin.to_date).to_i
  end

  def total_price_for_night
    self.orders.inject(0){ |total, item | total += item.quantity.to_i * item.room.price.to_i }
  end

  def price_total
    self.total_price_for_night*reservation_days
  end

  def orders_filter
    self.orders.each do |order|
      if order.quantity.blank?
        self.orders.delete(order)
      end
    end
  end

  def valid_date_range_required
    unless self.checkin.blank? or self.checkout.blank?
      errors.add(:base, "El check-out no puede ser despues del check-in") if self.checkin > self.checkout
    end
  end

  def valid_check_in
    unless self.checkin.blank? or self.checkout.blank?
      errors.add(:base, "El check-in que usted ingresó no es valido") if self.checkin < Date.parse(Time.new.to_s)
      errors.add(:base, "El check-in que usted ingresó no es valido") if self.checkout < Date.parse(Time.new.to_s)
    end
  end

  def validates_rooms
    room_hash = []
    self.orders.each do |order|
      unless order.quantity.blank?
        room_hash << order.quantity
      end
    end
    if room_hash.blank?
      errors.add(:base, "Debe elegir una habitación")
    end
  end

  def validate_fields
    errors.add(:base, "Debe elegir una forma de pago") if self.payment == 0
    errors.add(:base, "El N° de Niños debe ser mayor a cero") if self.kids.to_i < 0
    errors.add(:base, "El N° de Bebés debe ser mayor a cero") if self.babies.to_i < 0
    errors.add(:base, "El N° de Adultos debe ser mayor a cero") if self.adults.to_i < 1
  end

  #Se buscó el modelo "Client" por ActiveRecord ya que "Client" es un módulo reservado por ElasticSearch
  def verify_client
    if self.client.valid?
      client = ActiveRecord::Base.descendants.select { |model| model.to_s == "Client" } .first.find_by_documentation(self.client.documentation)

      if client
        client.update_attributes self.client.as_json only: [:name, :company, :email, :telephone]
        self.client = client
      end
    end
  end

  def status(checkin, checkout)
    checkin = Date.parse(checkin)
    checkout = Date.parse(checkout)
    time_now = Date.parse(Time.new.to_s)

    if  time_now < checkin
      return "#DC7B31"
    elsif time_now >= checkin and time_now <= checkout
      return "#0ABB33"
    else
      return "#EC0B0B"
    end
  end

end
#Reservation.import
