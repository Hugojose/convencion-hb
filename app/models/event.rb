#Generado por keppler
require 'elasticsearch/model'
class Event < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :client
  accepts_nested_attributes_for :client
  before_validation :verify_client

  validates_presence_of :begin_date, :end_date, :event_type

  #validacion de cantidad de personas
  validates :quantity, :numericality => { :less_than_or_equal_to => 100 }

  #custom validate
  validate :end_date_greater_than_begin_date?
  validate :assembly_type_not_blank

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end

  def chosen(selection)
    if selection == true
      return "Si"
    else
      return "No"
    end
  end

  def blank(text)
    if text.blank?
      return "N/A"
    end
  end

  def end_date_greater_than_begin_date?
    unless self.begin_date.blank? or self.end_date.blank?
      errors.add(:base, "La fecha de inicio no puede ser despues de la fecha de fin") if self.begin_date > self.end_date
    end
  end

  def assembly_type_not_blank
    errors.add(:base, 'Debe seleccionar un tipo de montaje') unless self.assembly_type
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:date_event, :event_type, :quantity, :begin_date, :end_date, :begin_time, :end_time, :assembly_type, :event_description, :client_name, :client_documentation, :client_telephone, :client_email, :client_company] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      client_name: self.client.name,
      client_documentation: self.client.documentation,
      client_telephone: self.client.telephone,
      client_email: self.client.email,
      client_company: self.client.company,
      date_event: self.date_event.to_s,
      event_type: self.event_type,
      quantity: self.quantity.to_s,
      begin_date: self.begin_date.to_s,
      end_date: self.end_date.to_s,
      begin_time: self.begin_time.to_s,
      end_time: self.end_time.to_s,
      assembly_type: self.assembly_type,
      event_description: self.event_description,
    }.as_json
  end

  #Se buscó el modelo "Client" por ActiveRecord ya que "Client" es un módulo reservado por ElasticSearch
  def verify_client
    if self.client.valid?
      client = ActiveRecord::Base.descendants.select { |model| model.to_s == "Client" } .first.find_by_documentation(self.client.documentation)

      if client
        client.update_attributes self.client.as_json only: [:name, :company, :email, :telephone]
        self.client = client
      end
    end
  end

  def status(begin_date, end_date)
    begin_date = Date.parse(begin_date)
    end_date = Date.parse(end_date)
    time_now = Date.parse(Time.new.to_s)

    if  time_now < begin_date
      return "#DC7B31"
    elsif time_now >= begin_date and time_now <= end_date
      return "#0ABB33"
    else
      return "#EC0B0B"
    end
  end

end
#Event.import
