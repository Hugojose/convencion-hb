$(document).ready(function(){
	$('.room-carousel').slick({
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: false,
		pauseOnHover: false
	});
	$('.rooms-collapse').on('shown.bs.collapse', function () {
		$('.other-rooms-carousel').slick({
			infinite: true,
			autoplay: true,
			autoplaySpeed: 5000,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	})
});