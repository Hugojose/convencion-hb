function removeParams () {
	history.pushState("", document.title, window.location.href.split('?')[0]);
}
function setTexts(){
	rooms = 0;
	night = 0;

	$('.quantity').each(function() {
		id = $(this).attr('id').replace('id-', '#id-')

		rooms += Number($(id).find('input').val());
		night += (Number($(id.replace('#id-', '#price-')).text().replace('Bs', '')) * Number($(id).find('input').val()))
	});

	$('#num-rooms').empty().append(rooms);
	$('#price-night').empty().append(night+" Bs");

	if (($("#checkin").val() == "") || ($("#checkout").val() == "") || ($('#checkin').datepicker('getDate') > $('#checkout').datepicker('getDate')) ){
		$('#price-total').empty().append("Debe seleccionar fechas válidas en Check-in y Check-out");
	}
	else{
		a = $("#checkin").datepicker('getDate').getTime();
		b = $("#checkout").datepicker('getDate').getTime();
		c = 24*60*60*1000;
		diffDays = Math.round(Math.abs((a - b)/(c)));

		$('#price-total').empty().append((night * diffDays)+" Bs");
	}
}
function quantity(id){
	$(document).ready(function(){
		if($("#id-"+id).hasClass('active')==false){
			$('#reservation_orders_attributes_'+(id)+'_quantity').prop('min', '0');
			$('#reservation_orders_attributes_'+(id)+'_room_id_'+id).prop('checked', true);
			$("#id-"+id).addClass( "active" );
			$("#mask-"+id).addClass( "red" );
			$("#id-"+id+" input").animate({
				opacity: 1,
				top: '10px'
			});
			$("#price-"+id).animate({
				top: '10px'
			});
		}else{
			$("#id-"+id).removeClass( "active" );
			$("#mask-"+id).removeClass( "red" );
			$('#reservation_orders_attributes_'+(id)+'_room_id_'+id).prop('checked', false);
			$("#id-"+id+" input").animate({
				opacity: 0,
				top: '-10px'
			}, 500, function(){
				$("#id-"+id+" input").val("");
			});
			$("#price-"+id).animate({
				top: '-30px'
			});
		}
	});
}

$(document).ready(function(){
	removeParams();

	$('.datepicker').datepicker({
		dayViewHeaderFormat: 'YYYY',
		format: 'dd-mm-yyyy',
		startDate: 'm',
		autoclose: 'true',
		todayBtn: 'linked',
		language: 'es'
	});

	$('.room-checks').click(function() {
		setTexts();
	});
	$('#checkin').change(function(){
		setTexts();
	});
	$('#checkout').change(function(){
		setTexts();
	});
});

function hideFormAlert() {
	$.when($('.form-alert').hide('slow')).done(function(){
		$('.form-alert').remove();
	});
}