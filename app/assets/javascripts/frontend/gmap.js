var map;
var markersArray = [];
var latlng = new google.maps.LatLng(8.6133326,-71.1453669);

function initialize()
{  
  var myOptions = {
      zoom: 17,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };

  map = new google.maps.Map(document.getElementById("map"), myOptions);
  placeMarker(latlng, map, "Calle Monagas, Sector La Puntica, Villa Marina, Falcón, Venezuela");
}

function placeMarker(location, map, title) {    
	var marker = new google.maps.Marker({
	    position: location, 
	    map: map,
	    animation: google.maps.Animation.BOUNCE,
	    title: title,
	    icon: "/assets/frontend/favicon.png"
	});
	// add marker in markers array
	markersArray.push(marker);	
}


google.maps.event.addDomListener(window, 'load', initialize);
